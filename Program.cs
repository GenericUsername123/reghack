﻿using Microsoft.VisualBasic.FileIO;
using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;

namespace RegHack
{
  static class Program
  {
    #region fields
    public static string AppDataFolder = Environment.GetFolderPath(Environment.
      SpecialFolder.ApplicationData) + "\\" + Assembly.GetExecutingAssembly().GetName().Name;
    public static string RegBackupFolder = AppDataFolder + @"\RegBackup\";
    internal static string lang = CultureInfo.CurrentCulture.ToString();
    internal static errCodes errCode = errCodes.unknown;
    internal static string[] clonedArgs = null;

    //Exprs
    public enum ArgTypes : int { SArgs = 0, AArgs = 1, SpArgs = 2 }
    public static string[][] Args =
    {
      new string[] { "link", "tkown", "backup", null, "clean", "prettify" },
      new string[] { "editor", "resextr", "img", "text", "recover", "export" },
      new string[] { "?", "pause", "pass" }
    };
    public enum AttrTypes : int { BAttrs = 0, PAttrs = 1, SpAttrs = 2 }
    public static string[][] Attrs =
    {
      new string[] { "e", "d" },
      new string[] { "s", "i" },
      new string[] { "undo", "all" }
    };
    //User provoked
    internal static bool[] setting = { true },
      display = { true, true, false };
    internal enum userProvoked
    {
      //Display
      error = 0, help = 1, logOff = 2,
      //Setting
      backup = 0
    }
    #endregion

    static void Main(string[] exprs)
    {
      bool debug = false;
      //Hotfix
      for (int iArgs = 0; iArgs < exprs.Length; iArgs++)
        if (exprs[iArgs].Contains("\"")) { helpParsed(); return; }
      //clsProgrammingTools.RemoveEscape(ref args);
      //End
      clonedArgs = clsProgrammingTools.LowerCase(ref exprs);
      //Checks for special arguments
      if (exprs != null)
      {
        string errExpr = null;
        if (Array.IndexOf(exprs, "/" + Args[2][1]) != -1) debug = true;
        if (Array.IndexOf(exprs, "/" + Args[2][0]) != -1) helpParsed();
        else errExpr = processExprs(exprs);
        if (errExpr != null) displayErr(errExpr);
        //Manage error recommendation
        if (errCode != errCodes.unknown) Console.WriteLine("\nFor help type: /" + Args[2][0]);
        //Recommendation
        if (display[(int)userProvoked.logOff]) Console.WriteLine(clsRegHack.GetText(clsRegHack.Option.logOff)[0]);
        //Pause script when debug is enabled
        if (debug) { Console.Write("\nPress any key to exit..."); Console.ReadKey(true); }
      }
      else helpParsed();
    }

    internal static string processExprs(string[] exprs)
    {
      if (Array.IndexOf(exprs, "/" + Args[2][2]) == -1)
      {
        //disclaimer
        string hr = "";
        for (int iHrLength = 0; iHrLength < 80; iHrLength++) hr += "-";
        Console.WriteLine(typeof(Program).Namespace + " is case-sensitive!\nTo make sure no files will be overwritten"
          + " check if all path attributes are exact\n\nAre you sure you want to continue? [y/n]\n" + hr + "\n");
        ConsoleKey ck;
        while (true)
        {
          ck = Console.ReadKey(true).Key;
          if (ck == ConsoleKey.Y || ck == ConsoleKey.N) if (ck == ConsoleKey.N) return null; else break;
        }
      }
      bool sArg = true;
      string arg = null, errExpr = null;
      string[] escape = { "/", ":" };
      int index = 0;
      for (int iExpr = 0; iExpr < exprs.Length; iExpr++)
      {
        index = iExpr + 1;
        if (exprs[iExpr].StartsWith(escape[0]) && exprs[iExpr].Length != 1)
        {
          arg = exprs[iExpr].Substring(1);
          display[(int)userProvoked.error] = true;
          //Simple args
          if (arg.Contains(escape[1]))
          {
            if (arg.Substring(0, arg.IndexOf(escape[1])) == Args[0][0]) errExpr = linkParsed(arg);
            else if (arg.Substring(0, arg.IndexOf(escape[1])) == Args[0][1]) errExpr = tkownParsed(arg);
            else if (arg.Substring(0, arg.IndexOf(escape[1])) == Args[0][2]) errExpr = backupParsed(arg);
            else if (arg.Substring(0, arg.IndexOf(escape[1])) == Args[0][4]) errExpr = cleanParsed(arg);
            else if (arg.Substring(0, arg.IndexOf(escape[1])) == Args[0][5]) errExpr = casingParsed(arg);
            else { errCode = errCodes.invalidArg; return exprs[iExpr]; }
          }
          else
          {
            //Simple args
            if (Array.IndexOf(Args[2], arg) != -1) sArg = true;
            else if (arg == Args[0][0]) errExpr = linkParsed(arg);
            else if (arg == Args[0][1]) errExpr = tkownParsed(arg);
            else if (arg == Args[0][2]) errExpr = backupParsed(arg);
            else if (arg == Args[0][4]) errExpr = cleanParsed(arg);
            else if (arg == Args[0][5]) errExpr = casingParsed(arg);
            else sArg = false;
            if (!sArg)
            {
              //Check attrs
              string[] attrs = null;
              for (int iNextExpr = iExpr + 1; iNextExpr < exprs.Length; iNextExpr++)
              {
                if (exprs[iNextExpr].Length > 2)
                {
                  for (int iAttrs = 0; iAttrs < Attrs[1].Length; iAttrs++)
                    if (Attrs[1][iAttrs] + "=" == exprs[iNextExpr].Remove(2))
                      exprs[iNextExpr] = clonedArgs[iNextExpr].Remove(2) + clonedArgs[iNextExpr].Substring(2);
                  if (!exprs[iNextExpr].StartsWith(escape[0]))
                  {
                    Array.Resize(ref attrs, iNextExpr - iExpr);
                    attrs[iNextExpr - iExpr - 1] = exprs[iNextExpr];
                  }
                  //iNextExpr = all expr, but current
                  else if (iNextExpr != iExpr + 1) { iExpr = iNextExpr - 1; break; } else break;
                  if (iNextExpr - iExpr == exprs.Length - iExpr - 1) { iExpr = exprs.Length; break; }
                }
                else { errCode = errCodes.invalidUse; return exprs[iNextExpr]; }
              }
              //Advanced args
              if (attrs == null) helpParsed(arg);
              else if (arg == Args[1][0]) errExpr = editorParsed(arg, attrs);
              else if (arg == Args[1][1]) errExpr = resextrParsed(arg, attrs);
              else if (arg == Args[1][2]) errExpr = imgParsed(arg, attrs);
              else if (arg == Args[1][3]) errExpr = textParsed(arg, attrs);
              else if (arg == Args[1][4]) errExpr = recoverParsed(arg, attrs);
              else if (arg == Args[1][5]) errExpr = exportParsed(arg, attrs);
              else { errCode = errCodes.invalidArg; return arg; }
            }
          }
          if (display[(int)userProvoked.error] && arg != Args[2][1] && arg != Args[2][2])
          { errCode = errCodes.invalidArg; return exprs[iExpr]; }
        }
        else { errCode = errCodes.invalidExpr; return exprs[iExpr]; }
        if (errExpr != null) return errExpr;
      }
      return null;
    }

    #region Special processes
    internal enum errCodes
    {
      unknown, invalidExpr, invalidArg, invalidAttr, invalidUse, invalidUseAttr,
      invalidFilepath, invalidFiletype
    }

    internal static void helpParsed(string arg = "?")
    {
      if (arg == "?")
      {
        display[(int)userProvoked.error] = false;
        string[][] descriptions = { clsRegHack.GetText(clsRegHack.Option.descriptionsS),
          clsRegHack.GetText(clsRegHack.Option.descriptionsA) };
        Console.WriteLine("Multiple arguments can be used at once.");
        Console.WriteLine("\nSimple arguments:\n");
        for (int iArgs = 0; iArgs < Args[0].Length; iArgs++)
          if (Args[0][iArgs] != null) Console.WriteLine("- /" + Args[0][iArgs] + "\t" + descriptions[0][iArgs]);
        Console.WriteLine("\nArguments: <arg>:<option>\tOptions: enable (e) / disable (d)\n\t\t\t" +
          "\tExample: /" + Args[0][0] + ":d\n");
        Console.WriteLine("Advanced arguments:\n");
        for (int iArgs = 0; iArgs < Args[1].Length; iArgs++)
          if (Args[1][iArgs] != null) Console.WriteLine("- /" + Args[1][iArgs] + "\t" + descriptions[1][iArgs]);
        Console.WriteLine("\nAttributes of advanced arguments are shown whenever only the argument " +
          "is passed.\nRegistry entries can be set to default using: your argument " + Attrs[2][0] + "\nExample: /" +
          Args[1][0] + " " + Attrs[2][0]);
      }
      else
      {
        for (int iArg = 0; iArg < Args[1].Length; iArg++)
        {
          if (arg == Args[1][iArg])
          {
            display[(int)userProvoked.error] = false;
            if (display[(int)userProvoked.help] && arg != Args[1][4] && arg != Args[1][5])
              Console.WriteLine("\nRegistry entries can be set to default using: your argument " + Attrs[2][0] +
                "\nExample: /" + Args[1][0] + " " + Attrs[2][0] + "\n");
            display[(int)userProvoked.help] = false;
          }
        }
        //Advanced arguments help
        if (arg == Args[2][2]) return;
        else if (arg == Args[1][0])
        {
          string[] attrs = { Attrs[1][0], Attrs[0][0], Attrs[0][1], Attrs[1][1] };
          var descriptions = new string[4];
          Array.Copy(clsRegHack.GetText(clsRegHack.Option.descriptions), descriptions, 4);
          Console.WriteLine("Attributes for " + Args[1][0] + ":\n");
          for (int iAttr = 0; iAttr < attrs.Length - 1; iAttr++)
          {
            if (iAttr == 0) Console.Write("Essenstial:\t");
            else Console.Write("\t\t");
            Console.WriteLine("- " + attrs[iAttr] + "\t" + descriptions[iAttr]);
          }
          Console.WriteLine("Optional:\t" + "- " + attrs[attrs.Length - 1] + "\t" +
            descriptions[descriptions.Length - 1] + "\n\nAttribute types:\n");
          for (int iAttr = 0; iAttr < attrs.Length; iAttr++)
          {
            if (iAttr % 3 == 0)
              Console.WriteLine("path\t\t- " + attrs[iAttr] + "\t" +
              "Specify the location of this object.");
            else
              Console.WriteLine("bool\t\t- " + attrs[iAttr] + "\t" +
              "Specify if this option should be enabled or disabled.");
          }
          Console.WriteLine("\nExample: /" + Args[1][0] + " s=\"C:\\source\\source.exe\" i=\"C:\\source\\" +
            "source.ico\" f:e d:d");
        }
        else if (arg == Args[1][1])
        {
          string[] attrs = { Attrs[1][0], Attrs[1][1] };
          var descriptions = new string[2];
          Array.Copy(clsRegHack.GetText(clsRegHack.Option.descriptions), descriptions, 2);
          Console.WriteLine("Attributes for " + Args[1][1] + ":\n");
          for (int iAttr = 0; iAttr < attrs.Length; iAttr++)
          {
            if (iAttr % 2 == 0) Console.Write("Essential:\t");
            else Console.Write("Optional:\t");
            Console.WriteLine("- " + attrs[iAttr] + "\t" + descriptions[iAttr]);
          }
          Console.WriteLine("\nAttribute types:\n");
          for (int iAttr = 0; iAttr < attrs.Length; iAttr++)
          { Console.WriteLine("path\t\t- " + attrs[iAttr] + "\t" + descriptions[iAttr]); }
          Console.WriteLine("\nExample: /" + Args[1][1] + " s=\"C:\\source\\source.exe\" i=\"C:\\source\\source.ico\"");
        }
        else if (arg == Args[1][2])
        {
          string attrs = Attrs[1][0];
          string descriptions = clsRegHack.GetText(clsRegHack.Option.descriptions)[3];
          Console.WriteLine("Attributes for " + Args[1][2] + ":\n");
          for (int iAttr = 0; iAttr < attrs.Length; iAttr++)
          { Console.WriteLine("path\t\t- " + attrs + "\t" + descriptions); }
          Console.WriteLine("\nExample: /" + Args[1][2] + " s=\"C:\\source\\source.exe\"");
        }
        else if (arg == Args[1][3])
        {
          string attrs = Attrs[1][0];
          string descriptions = clsRegHack.GetText(clsRegHack.Option.descriptions)[6];
          Console.WriteLine("Attributes for " + Args[1][3] + ":\n");
          for (int iAttr = 0; iAttr < attrs.Length; iAttr++)
          { Console.WriteLine("path\t\t- " + attrs + "\t" + descriptions); }
          Console.WriteLine("\nExample: /" + Args[1][3] + " s=\"C:\\source\\source.exe\"");
        }
        else if (arg == Args[1][4])
        {
          Console.WriteLine("\nRecover using the last created backups saved.\n\nAttributes for " + Args[1][4] + ":\n");
          string[] keyCalls = { "hkcr", "hkcu", "hklm" };
          string extraChar = null;
          string expr = null;
          string tab = null;
          int iNull = 1;
          for (int iArgs = 0; iArgs < Args[0].Length + Args[1].Length + keyCalls.Length; iArgs++)
          {
            if (iArgs < Args[0].Length) expr = Args[0][iArgs];
            else if (iArgs < Args[0].Length + Args[1].Length) expr = Args[1][iArgs - Args[0].Length];
            else expr = keyCalls[iArgs - Args[0].Length - Args[1].Length];
            if (expr != null)
            {
              if ((iArgs + iNull) % 4 == 0) extraChar = "\n";
              else
              {
                for (int i = 0; i < 20; i++) if (i > expr.Length) tab += " ";
                extraChar = tab;
              }
              Console.Write(expr + extraChar);
              tab = null;
            }
            else iNull--;
          }
          Console.WriteLine("\n\n\nExample: /" + Args[1][4] + " " + Args[0][0] + " " + Args[1][0] +
            "\n\nOr recover using all last created backups using: " + Attrs[2][1]);
        }
        else if (arg == Args[1][5])
        {
          string[][] keys = { new string[] { "hkcr", "hkcu", "hklm" }, new string[]
            { Registry.ClassesRoot.Name, Registry.CurrentUser.Name, Registry.LocalMachine.Name } };
          Console.WriteLine("Recover using the last created backups saved.\n\nAttributes for " + Args[1][5] + ":\n");
          for (int iArg = 0; iArg < keys[0].Length; iArg++)
            Console.WriteLine(keys[0][iArg] + "\t\t\tCreate a backup of " + keys[1][iArg] + ".");
          Console.WriteLine("\nExample: /" + Args[1][5] + " " + keys[0][0] + " " + keys[0][2]);
        }
        else { errCode = errCodes.unknown; displayErr(null); return; }
        Console.WriteLine();
      }
    }

    internal static void displayErr(string expr = "the expression used")
    {
      switch (errCode)
      {
        case errCodes.invalidExpr:
          Console.WriteLine(expr + " is not a valid expression!");
          break;
        case errCodes.invalidArg:
          Console.WriteLine(expr + " is not a valid argument!");
          break;
        case errCodes.invalidAttr:
          Console.WriteLine(expr + " is not a valid attribute!");
          break;
        case errCodes.invalidUse:
          Console.WriteLine("Invalid attribute or use of attribute " + expr + ".");
          break;
        case errCodes.invalidUseAttr:
          Console.WriteLine("Invalid use of attributes for " + expr + ".");
          break;
        case errCodes.invalidFilepath:
          Console.WriteLine("Invalid filepath sent using " + expr + ".");
          break;
        case errCodes.invalidFiletype:
          Console.WriteLine("Invalid filetype sent using " + expr + ".");
          break;
        case errCodes.unknown:
          Console.WriteLine("An unknown exception occured!");
          break;
      }
    }
    #endregion

    #region Simple processes
    internal static string linkParsed(string arg)
    {
      display[(int)userProvoked.error] = false;
      string SArg = Args[0][0];
      //Do not change!
      string subKeyName = "link";
      RegistryKey explorer = Registry.CurrentUser.OpenSubKey(
        @"Software\Microsoft\Windows\CurrentVersion\Explorer", true);
      //Processing settings
      if (arg != SArg && arg != SArg + ":" + Attrs[0][0] && arg != SArg + ":" + Attrs[0][1])
      { errCode = errCodes.invalidUseAttr; return SArg; }
      else
      {
        if (setting[(int)userProvoked.backup]) backupKeys(SArg, new RegistryKey[] { explorer }, subKeyName);
        //Applying settings
        if (arg.Contains(Attrs[0][0]) || arg == SArg)
        {
          Console.Write("Restoring shortcut text... ");
          explorer.SetValue(subKeyName, new byte[] { 30, 00, 00, 00 }, RegistryValueKind.Binary);
        }
        else
        {
          Console.Write("Removing shortcut text... ");
          explorer.SetValue(subKeyName, new byte[] { 00, 00, 00, 00 }, RegistryValueKind.Binary);
        }
        explorer.Close();
        Console.WriteLine("Done!");
        display[(int)userProvoked.logOff] = true;
      }
      return null;
    }

    internal static string tkownParsed(string arg)
    {
      display[(int)userProvoked.error] = false;
      string SArg = Args[0][1];
      //Do not change!
      string subKeyName = "tkown";
      //Translate
      string tkown;
      if (lang == "nl-NL" || lang == "nl-BE") tkown = "Eigenaar worden";
      else tkown = "Take ownership";
      //Processing settings
      if (arg != SArg && arg != SArg + ":" + Attrs[0][0] && arg != SArg + ":" + Attrs[0][1])
      { errCode = errCodes.invalidUseAttr; return SArg; }
      else
      {
        RegistryKey[] keys = { Registry.ClassesRoot.OpenSubKey(@"*\shell", true),
          Registry.ClassesRoot.OpenSubKey(@"Directory\shell", true) };
        if (setting[(int)userProvoked.backup]) backupKeys(SArg, keys, subKeyName);
        //Applying settings
        string regHackIco = @"C:\Windows\Resources\RegHacksIcons\";
        if (arg.Contains(Attrs[0][0]) || arg == SArg)
        {
          Console.Write("Adding \"{0}\" option... ", tkown);
          //Dir
          if (!Directory.Exists(regHackIco)) Directory.CreateDirectory(regHackIco);
          var ms = new MemoryStream();
          Properties.Resources.UAC.Save(ms);
          File.WriteAllBytes(regHackIco + "UAC.ico", ms.ToArray());
          //Reg
          keys[0] = keys[0].CreateSubKey(subKeyName);
          keys[0].SetValue(null, tkown);
          keys[0].SetValue("NoWorkingDirectory", "");
          keys[0].SetValue("Icon", regHackIco + "UAC.ico");
          keys[0] = keys[0].CreateSubKey("command");
          keys[0].SetValue(null, clsRegHack.GetText(clsRegHack.Option.tkown)[0]);
          keys[0].SetValue("IsolatedCommand", clsRegHack.GetText(clsRegHack.Option.tkown)[0]);
          keys[0].Dispose();
          keys[1] = keys[1].CreateSubKey(subKeyName);
          keys[1].SetValue(null, tkown);
          keys[1].SetValue("NoWorkingDirectory", "");
          keys[1].SetValue("Icon", regHackIco + "UAC.ico");
          keys[1] = keys[1].CreateSubKey("command");
          keys[1].SetValue(null, clsRegHack.GetText(clsRegHack.Option.tkown)[1]);
          keys[1].SetValue("IsolatedCommand", clsRegHack.GetText(clsRegHack.Option.tkown)[1]);
          keys[0].Close();
          keys[1].Close();
          Console.WriteLine("Done!");
        }
        else
        {
          Console.Write("Removing \"{0}\" option... ", tkown);
          //Dir
          if (File.Exists(regHackIco + "UAC.ico")) File.Delete(regHackIco + "UAC.ico");
          //Check for empty folder
          var dirInfo = new DirectoryInfo(regHackIco);
          if (dirInfo.Exists)
            if (dirInfo.GetFiles().Length == 0 && dirInfo.GetDirectories().Length == 0)
              dirInfo.Delete();
          //Reg
          if (keys[0].OpenSubKey(subKeyName) != null) keys[0].DeleteSubKeyTree(subKeyName);
          if (keys[1].OpenSubKey(subKeyName) != null) keys[1].DeleteSubKeyTree(subKeyName);
          keys[0].Close();
          keys[1].Close();
          Console.WriteLine("Done!");
        }
      }
      return null;
    }

    internal static string backupParsed(string arg)
    {
      display[(int)userProvoked.error] = false;
      string SArg = Args[0][2];
      //Processing settings
      if (arg != SArg && arg != SArg + ":" + Attrs[0][0] && arg != SArg + ":" + Attrs[0][1])
      { errCode = errCodes.invalidUseAttr; return SArg; }
      else
      {
        //Applying settings
        if (arg.Contains(Attrs[0][0]) || arg == SArg)
        { setting[(int)userProvoked.backup] = true; Console.WriteLine("Backups are enabled!"); }
        else
        { setting[(int)userProvoked.backup] = false; Console.WriteLine("Backups are disabled!"); }
      }
      return null;
    }

    internal static string cleanParsed(string arg)
    {
      display[(int)userProvoked.error] = false;
      string SArg = Args[0][4];
      //Processing settings
      if (arg != SArg && arg != SArg + ":" + Attrs[0][0] && arg != SArg + ":" + Attrs[0][1])
      { errCode = errCodes.invalidUseAttr; return SArg; }
      else
      {
        //Applying settings
        if (arg.Contains(Attrs[0][0]) || arg == SArg)
        {
          if (Directory.Exists(RegBackupFolder))
          {
            Console.Write("Deleting old backups... ");
            //Scan for filenames
            FileInfo[] files = new DirectoryInfo(RegBackupFolder).GetFiles();
            var filesFilter = new FileInfo[0];
            var fileNames = new string[files.Length];
            string fileName = null;
            for (int iFiles = 0; iFiles < files.Length; iFiles++)
            {
              fileName = files[iFiles].Name;
              if (fileName.Contains(".")) fileName = fileName.Substring(files[iFiles].Name.IndexOf(".") + 1);
              if (fileName.Length > 3) fileName = fileName.Remove(fileName.Length - 4);
              fileNames[iFiles] = fileName;
            }
            //Scan file per filename
            for (int iFileNames = 0; iFileNames < fileNames.Length; iFileNames++)
            {
              for (int iFiles = 0; iFiles < files.Length; iFiles++)
              {
                if (files[iFiles].Name.Contains(fileNames[iFileNames]))
                {
                  Array.Resize(ref filesFilter, filesFilter.Length + 1);
                  filesFilter[filesFilter.Length - 1] = files[iFiles];
                }
              }
              //Delete older files
              for (int iFiles = 0; iFiles < filesFilter.Length - 1; iFiles++)
              {
                if (filesFilter[iFiles + 1].CreationTime < filesFilter[iFiles].CreationTime)
                  fileName = filesFilter[iFiles + 1].FullName;
                else fileName = filesFilter[iFiles].FullName;
                if (File.Exists(fileName))
                  FileSystem.DeleteFile(fileName, UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin);
              }
              filesFilter = new FileInfo[0];
            }
            Console.WriteLine("Done!");
          }
          else Console.WriteLine("No backups are present!");
        }
      }
      return null;
    }

    internal static string casingParsed(string arg)
    {
      display[(int)userProvoked.error] = false;
      string SArg = Args[0][5];
      //Do not change!
      string subKeyName = "DontPrettyPath";
      RegistryKey advanced = Registry.CurrentUser.OpenSubKey(
        @"Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced", true);
      //Processing settings
      if (arg != SArg && arg != SArg + ":" + Attrs[0][0] && arg != SArg + ":" + Attrs[0][1])
      { errCode = errCodes.invalidUseAttr; return SArg; }
      else
      {
        if (setting[(int)userProvoked.backup]) backupKeys(SArg, new RegistryKey[] { advanced }, subKeyName);
        //Applying settings
        string attr = arg.Substring(arg.Length - 1);
        if (attr == Attrs[0][0] || arg == SArg)
        {
          Console.Write("Enabling prettifying feature... ");
          advanced.SetValue(subKeyName, 0, RegistryValueKind.DWord);
        }
        else
        {
          Console.Write("Disabling prettifying feature... ");
          advanced.SetValue(subKeyName, 1, RegistryValueKind.DWord);
        }
        advanced.Close();
        Console.WriteLine("Done!");
        display[(int)userProvoked.logOff] = true;
      }
      return null;
    }
    #endregion

    #region Advanced processes
    internal static string editorParsed(string arg, string[] attrs)
    {
      display[(int)userProvoked.error] = false;
      string subKeyName = "editor";
      bool sourcePresent = false;
      bool fileOrDirPresent = false;
      string attrValue = null;
      //Check for disable attr
      if (Array.IndexOf(attrs, Attrs[2][0]) != -1)
      {
        attrs = new string[3];
        attrs[0] = Attrs[1][0] + @"=C:\WINDOWS\System32\notepad.exe";
        attrs[1] = "f:" + Attrs[0][1];
        attrs[2] = "d:" + Attrs[0][1];
      }
      //Check if all needed attrs are present
      if (attrs.Length >= 2)
      {
        var attrValues = new string[4];
        for (int iAttrs = 0; iAttrs < attrs.Length; iAttrs++)
        {
          attrValue = attrs[iAttrs].Substring(2);
          if (attrs[iAttrs].StartsWith(Attrs[1][0] + "=")) { sourcePresent = true; attrValues[0] = attrValue; }
          else if (attrs[iAttrs].StartsWith("f:") || attrs[iAttrs].StartsWith("d:"))
          {
            fileOrDirPresent = true;
            if (attrs[iAttrs].StartsWith("f:")) attrValues[2] = attrValue;
            else attrValues[3] = attrValue;
          }
          else if (attrs[iAttrs].StartsWith(Attrs[1][1] + "=")) attrValues[1] = attrValue;
          else { errCode = errCodes.invalidAttr; return attrs[iAttrs]; }
        }
        //Needed attrs not present
        if (!sourcePresent || !fileOrDirPresent) { errCode = errCodes.invalidUseAttr; return arg; }
        //Processing settings
        RegistryKey[] keys = { Registry.ClassesRoot.OpenSubKey(@"*\shell", true),
          Registry.ClassesRoot.OpenSubKey(@"Directory\shell", true) };
        string errExpr = clsRegHack.CheckForFiles(arg, new string[] { attrValues[0], attrValues[1] },
          new string[] { "exe", "ico" }, new int[] { 1 });
        if (errExpr != null) return errExpr;
        string source = attrValues[0];
        string icon = attrValues[1];
        string file = attrValues[2];
        string folder = attrValues[3];
        if (!(file == Attrs[0][0] || file == Attrs[0][1] || file == null) ||
          !(folder == Attrs[0][0] || folder == Attrs[0][1] || folder == null))
        { errCode = errCodes.invalidUseAttr; return arg; }
        if (setting[(int)userProvoked.backup]) backupKeys(arg, keys, subKeyName);
        //Translate
        string openWith;
        if (lang == "nl-NL" || lang == "nl-BE") openWith = "Openen met ";
        else openWith = "Open with ";
        //Applying settings
        Console.Write("Changing option from contextmenu... ");
        string editorName = "text editor";
        if (file == Attrs[0][0])
        {
          keys[0] = keys[0].CreateSubKey(subKeyName);
          keys[0].SetValue(null, openWith + editorName);
          if (icon != null) keys[0].SetValue("Icon", "\"" + icon + "\"");
          keys[0] = keys[0].CreateSubKey("command");
          keys[0].SetValue(null, "\"" + source + "\" \"%1\"");
          keys[0].Close();
        }
        else if (file == Attrs[0][1])
        {
          if (keys[0].OpenSubKey(subKeyName) != null) keys[0].DeleteSubKeyTree(subKeyName);
          keys[0].Close();
        }
        if (folder == Attrs[0][0])
        {
          keys[1] = keys[1].CreateSubKey(subKeyName);
          keys[1].SetValue(null, openWith + editorName);
          if (icon != null) keys[1].SetValue("Icon", "\"" + icon + "\"");
          keys[1] = keys[1].CreateSubKey("command");
          keys[1].SetValue(null, "\"" + source + "\" \"%1\"");
          keys[1].Close();
        }
        else if (folder == Attrs[0][1])
        {
          if (keys[1].OpenSubKey(subKeyName) != null) keys[1].DeleteSubKeyTree(subKeyName);
          keys[1].Close();
        }
        Console.WriteLine("Done!");
      }
      else { errCode = errCodes.invalidUseAttr; return arg; }
      return null;
    }

    internal static string resextrParsed(string arg, string[] attrs)
    {
      display[(int)userProvoked.error] = false;
      string subKeyName = "resextr";
      string attrValue = null;
      RegistryKey[] keys =
      { Registry.ClassesRoot.OpenSubKey(@"exefile\shell", true),
        Registry.ClassesRoot.OpenSubKey("dllfile", true).CreateSubKey("shell"),
        Registry.ClassesRoot.OpenSubKey("ocxfile", true).CreateSubKey("shell"),
        Registry.ClassesRoot.OpenSubKey(@"cplfile\shell", true) };
      //Check for disable attr
      if (Array.IndexOf(attrs, Attrs[2][0]) != -1)
      {
        Console.Write("Removing all entries... ");
        for (int iKeys = 0; iKeys < keys.Length; iKeys++)
        {
          if (keys[iKeys].OpenSubKey(subKeyName) != null) keys[iKeys].DeleteSubKeyTree(subKeyName);
          keys[iKeys].Close();
        }
        Console.WriteLine("Done!");
        return null;
      }
      //Check if all needed attrs are present
      var attrValues = new string[2];
      for (int iAttrs = 0; iAttrs < attrs.Length; iAttrs++)
      {
        attrValue = attrs[iAttrs].Substring(2);
        if (attrs[iAttrs].StartsWith(Attrs[1][0] + "=")) attrValues[0] = attrValue;
        else if (attrs[iAttrs].StartsWith(Attrs[1][1] + "=")) attrValues[1] = attrValue;
        else { errCode = errCodes.invalidAttr; return attrs[iAttrs]; }
      }
      //Processing settings
      string errExpr = clsRegHack.CheckForFiles(arg, new string[] { attrValues[0], attrValues[1] },
        new string[] { "exe", "ico" }, new int[] { 1 });
      if (errExpr != null) return errExpr;
      string source = attrValues[0];
      string icon = attrValues[1];
      if (setting[(int)userProvoked.backup]) backupKeys(arg, keys, subKeyName);
      //Applying settings
      string altIcon = source.Replace(".exe", "_exported.ico");
      if (icon == null)
      {
        string iconName = altIcon.Substring(altIcon.LastIndexOf("\\") + 1);
        Console.WriteLine("Saving ResourcesExtract icon in root folder... ");
        if (!File.Exists(altIcon))
        {
          var ms = new MemoryStream();
          Properties.Resources.ResourcesExtract.Save(ms);
          File.WriteAllBytes(altIcon, ms.ToArray());
          Console.WriteLine("Saved as " + iconName + ".");
        }
        else Console.WriteLine(iconName + " exists already!");
      }
      Console.Write("Adding option to contextmenu... ");
      for (int iKeys = 0; iKeys < keys.Length; iKeys++)
      {
        keys[iKeys] = keys[iKeys].CreateSubKey(subKeyName);
        keys[iKeys].SetValue(null, "Extract resources");
        if (icon != null) keys[iKeys].SetValue("Icon", "\"" + icon + "\"");
        else keys[iKeys].SetValue("Icon", "\"" + altIcon + "\"");
        keys[iKeys] = keys[iKeys].CreateSubKey("command");
        keys[iKeys].SetValue(null, "\"" + source + "\" /Source \"%1\" /DestFolder \"extracted_resources\"");
        keys[iKeys].Close();
      }
      Console.WriteLine("Done!");
      return null;
    }

    internal static string imgParsed(string arg, string[] attrs)
    {
      display[(int)userProvoked.error] = false;
      RegistryKey[] keys =
      { Registry.ClassesRoot.OpenSubKey(@"jpegfile\shell\edit\command", true),
        Registry.ClassesRoot.OpenSubKey(@"pngfile\shell\edit\command", true) };
      //Check for disable attr
      if (Array.IndexOf(attrs, Attrs[2][0]) != -1)
      {
        Console.Write("Resetting all entries... ");
        for (int iKeys = 0; iKeys < keys.Length; iKeys++)
        {
          keys[iKeys].SetValue(null, "\"C:\\WINDOWS\\System32\\mspaint.exe\" \"%1\"");
          keys[iKeys].Close();
        }
        Console.WriteLine("Done!");
        return null;
      }
      for (int iAttrs = 0; iAttrs < attrs.Length; iAttrs++)
        if (!attrs[iAttrs].StartsWith(Attrs[1][0] + "=")) { errCode = errCodes.invalidAttr; return attrs[iAttrs]; }
      //Processing settings
      string source = attrs[attrs.Length - 1].Substring(2);
      string errExpr = clsRegHack.CheckForFiles(arg, new string[] { source }, new string[] { "exe" });
      if (errExpr != null) return errExpr;
      if (setting[(int)userProvoked.backup]) backupKeys(arg, keys);
      //Applying settings
      Console.Write("Changing values to source... ");
      for (int iKeys = 0; iKeys < keys.Length; iKeys++)
      {
        keys[iKeys].SetValue(null, "\"" + source + "\" \"%1\"");
        keys[iKeys].Close();
      }
      Console.WriteLine("Done!");
      return null;
    }

    internal static string textParsed(string arg, string[] attrs)
    {
      display[(int)userProvoked.error] = false;
      RegistryKey[] keys =
      { Registry.ClassesRoot.OpenSubKey(@"batfile\shell\edit\command", true),
        Registry.ClassesRoot.OpenSubKey(@"cmdfile\shell\edit\command", true),
        Registry.ClassesRoot.OpenSubKey(@"regfile\shell\edit\command", true),
        Registry.ClassesRoot.OpenSubKey(@"JSFile\shell\edit\Command", true),
        Registry.ClassesRoot.OpenSubKey(@"VBEFile\shell\edit\Command", true),
        Registry.ClassesRoot.OpenSubKey(@"VBSFile\shell\edit\Command", true),
        Registry.ClassesRoot.OpenSubKey(@"SystemFileAssociations\text\shell" +
        @"\edit\command", true) };
      //Check for disable attr
      if (Array.IndexOf(attrs, Attrs[2][0]) != -1)
      {
        Console.Write("Resetting all entries... ");
        for (int iKeys = 0; iKeys < keys.Length; iKeys++)
        {
          keys[iKeys].SetValue(null, "\"C:\\WINDOWS\\System32\\notepad.exe\" \"%1\"");
          keys[iKeys].Close();
        }
        Console.WriteLine("Done!");
        return null;
      }
      for (int iAttrs = 0; iAttrs < attrs.Length; iAttrs++)
        if (!attrs[iAttrs].StartsWith(Attrs[1][0] + "=")) { errCode = errCodes.invalidAttr; return attrs[iAttrs]; }
      //Processing settings
      string source = attrs[attrs.Length - 1].Substring(2);
      string errExpr = clsRegHack.CheckForFiles(arg, new string[] { source }, new string[] { "exe" });
      if (errExpr != null) return errExpr;
      if (setting[(int)userProvoked.backup]) backupKeys(arg, keys);
      //Applying settings
      Console.Write("Changing values to source... ");
      for (int iKeys = 0; iKeys < keys.Length; iKeys++)
      {
        keys[iKeys].SetValue(null, "\"" + source + "\" \"%1\"");
        keys[iKeys].Close();
      }
      Console.WriteLine("Done!");
      return null;
    }

    internal static string recoverParsed(string arg, string[] attrs)
    {
      display[(int)userProvoked.error] = false;
      //Processing settings
      bool recoverAll = false;
      bool sourcePresent = false;
      string[][] keys = { new string[] { "hkcr", "hkcu", "hklm", "cc", "u" },
        new string[] { Registry.ClassesRoot.Name, Registry.CurrentUser.Name,
          Registry.LocalMachine.Name, Registry.CurrentConfig.Name, Registry.Users.Name } };
      int namePos = 0;
      for (int iAttrs = 0; iAttrs < attrs.Length; iAttrs++)
      {
        if (attrs[iAttrs] != Attrs[2][1] || attrs.Length != 1)
        {
          sourcePresent = false;
          for (int iArgs = 0; iArgs < Args[0].Length + Args[1].Length; iArgs++)
          {
            if (iArgs <= Args[0].Length - 1) { if (Args[0][iArgs] == attrs[iAttrs]) sourcePresent = true; }
            else
            {
              if (Args[1][iArgs - Args[0].Length] == attrs[iAttrs]) sourcePresent = true;
              else
              {
                namePos = Array.IndexOf(keys[0], attrs[iAttrs]);
                if (namePos != -1) { sourcePresent = true; attrs[iAttrs] = keys[1][namePos]; }
              }
            }
          }
          //Needed attrs not present
          if (!sourcePresent) { errCode = errCodes.invalidUseAttr; return arg; }
        }
        else recoverAll = true;
      }
      //Applying settings
      if (Directory.Exists(RegBackupFolder))
      {
        Console.Write("Recovering from backups... ");
        bool recoverAny = false;
        //Scan for filenames
        FileInfo[] files = new DirectoryInfo(RegBackupFolder).GetFiles();
        var filesFilter = new FileInfo[0];
        var fileNames = new string[files.Length];
        string fileName = null;
        for (int iFiles = 0; iFiles < files.Length; iFiles++)
        {
          fileName = files[iFiles].Name;
          if (fileName.Contains(".")) fileName = fileName.Substring(files[iFiles].Name.IndexOf(".") + 1);
          if (fileName.Length > 3) fileName = fileName.Remove(fileName.Length - 4);
          fileNames[iFiles] = fileName;
        }
        //Make sure there are no repeats
        for (int iNames = 0; iNames < fileNames.Length; iNames++)
          if (Array.LastIndexOf(fileNames, fileNames[iNames]) != iNames) fileNames[iNames] = null;
        //Scan file per filename
        bool recoverSpecified = false;
        var regEdit = new Process();
        for (int iFileNames = 0; iFileNames < fileNames.Length; iFileNames++)
        {
          if (fileNames[iFileNames] != null)
          {
            recoverSpecified = false;
            for (int iFiles = 0; iFiles < files.Length; iFiles++)
              if (files[iFiles].Name.Contains(fileNames[iFileNames]))
              {
                Array.Resize(ref filesFilter, filesFilter.Length + 1);
                filesFilter[filesFilter.Length - 1] = files[iFiles];
              }
            //Recover most recent
            for (int iFilesFilter = 0; iFilesFilter < filesFilter.Length - 1; iFilesFilter++)
              if (filesFilter[iFilesFilter + 1].CreationTime < filesFilter[iFilesFilter].CreationTime)
                filesFilter[iFilesFilter + 1] = null;
              else filesFilter[iFilesFilter] = null;
            string file = null;
            for (int iFilesFilter = 0; iFilesFilter < filesFilter.Length; iFilesFilter++)
              if (filesFilter[iFilesFilter] != null) file = filesFilter[iFilesFilter].FullName;
            //Check if specified
            if (fileNames[iFileNames].Contains("."))
            {
              for (int iAttrs = 0; iAttrs < attrs.Length; iAttrs++)
                if (attrs[iAttrs] == fileNames[iFileNames].Remove(fileNames[iFileNames].IndexOf(".")))
                { recoverSpecified = true; break; }
              if (recoverAll || recoverSpecified)
              {
                recoverAny = true;
                regEdit.StartInfo = new ProcessStartInfo("regedit", "/s /i \"" + file + "\"");
                regEdit.Start();
                regEdit.WaitForExit();
              }
            }
          }
          filesFilter = new FileInfo[0];
        }
        if (!recoverAny) Console.WriteLine("No backup present!");
        else Console.WriteLine("Done!");
      }
      else Console.WriteLine("No backups are present!");
      return null;
    }

    internal static string exportParsed(string arg, string[] attrs)
    {
      display[(int)userProvoked.error] = false;
      string AArg = Args[1][5];
      RegistryKey[] keys = { Registry.ClassesRoot, Registry.CurrentUser,
        Registry.LocalMachine, Registry.CurrentConfig, Registry.Users };
      string[] keyCalls = { "hkcr", "hkcu", "hklm" };
      RegistryKey[] backup = { null };
      //Processing settings
      for (int iAttrs = 0; iAttrs < attrs.Length; iAttrs++)
      {
        if (Array.IndexOf(keyCalls, attrs[iAttrs]) == -1)
        { errCode = errCodes.invalidUseAttr; return arg; }
        else
        {
          for (int iArgs = 0; iArgs < keyCalls.Length; iArgs++)
            if (attrs[iAttrs] == keyCalls[iArgs] && Array.IndexOf(backup, keys[iArgs]) == -1)
            {
              if (backup[0] != null) Array.Resize(ref backup, backup.Length + 1);
              backup[backup.Length - 1] = keys[iArgs];
            }
        }
      }
      //Applying settings
      Console.WriteLine("Creating a backup of all specified entries...");
      backupKeys(arg, backup);
      for (int iKeys = 0; iKeys < keys.Length; iKeys++)
      {
        backup = keys;
        keys[iKeys].Close();
        backup[iKeys].Close();
      }
      return null;
    }
    #endregion

    #region backup
    internal static void createRegBackupFolder()
    {
      if (!Directory.Exists(RegBackupFolder)) Directory.CreateDirectory(RegBackupFolder);
      if (!File.Exists(AppDataFolder + "\\Readme.txt"))
        File.WriteAllText(AppDataFolder + "\\Readme.txt",
          clsRegHack.GetText(clsRegHack.Option.regBackupText)[0]);
    }

    internal static void backupKeys(string arg, RegistryKey[] keys = null, string subKeyName = null)
    {
      Console.Write("Creating a backup... ");
      createRegBackupFolder();
      RegistryKey[] temp = new RegistryKey[keys.Length];
      Array.Copy(keys, temp, keys.Length);
      bool noBackup = false;
      if (arg != Args[0][0] && arg != Args[0][5])
      {
        if (subKeyName != null)
          for (int iKeys = 0; iKeys < keys.Length; iKeys++)
            if (keys[iKeys].OpenSubKey(subKeyName) == null) noBackup = true;
            else keys[iKeys] = keys[iKeys].OpenSubKey(subKeyName, true);
        if (noBackup) { Console.WriteLine("There is nothing to backup!"); return; }
        var regEdit = new Process();
        for (int iKeys = 0; iKeys < keys.Length; iKeys++)
        {
          if (arg != Args[1][5])
            regEdit.StartInfo = new ProcessStartInfo("regedit", "/s /e \"" + RegBackupFolder +
            DateTime.Now.ToString().Replace("/", "").Replace(" ", "").Replace(":", "_") +
            "." + arg + "." + iKeys + ".reg" + "\" \"" + keys[iKeys].ToString() + "\"");
          else
            regEdit.StartInfo = new ProcessStartInfo("regedit", "/s /e \"" + RegBackupFolder +
            DateTime.Now.ToString().Replace("/", "").Replace(" ", "").Replace(":", "_") +
            "." + keys[iKeys].Name + "." + iKeys + ".reg" + "\" \"" + keys[iKeys].ToString() + "\"");
          regEdit.Start();
          regEdit.WaitForExit();
        }
      }
      else
      {
        string fileName = RegBackupFolder + DateTime.Now.ToString().Replace("/", "").
          Replace(" ", "").Replace(":", "_") + "." + arg + ".0" + ".reg";
        string text = "Windows Registry Editor Version 5.00" + Environment.NewLine + Environment.NewLine
          + "[" + keys[0].ToString() + "]" + Environment.NewLine + "\"" + subKeyName + "\"=";
        if (Args[0][0] == arg)
        {
          if (clsProgrammingTools.CompareByteArrays((byte[])keys[0].GetValue(subKeyName), new byte[] { 30, 00, 00, 00 }))
            text += "hex:e1,00,00,00";
          else text += "hex:00,00,00,00";
        }
        else if (Args[0][5] == arg)
        {
          if ((int)keys[0].GetValue(subKeyName) == 0) text += "dword:00000000";
          else text += "dword:00000001";
        }
        File.WriteAllText(fileName, text, System.Text.Encoding.Unicode);
      }
      Console.WriteLine("A backup has been made.\nDirectory: " + RegBackupFolder);
      Array.Copy(temp, keys, temp.Length);
    }
    #endregion
  }
}
