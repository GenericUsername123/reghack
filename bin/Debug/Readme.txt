﻿DO NOT run any hack that is already installed on your PC unless you know what you are doing!!!

No installation is necessary.

In order to use this program you will have to run command prompt from the root directory of this executable.
Arguments are followed by a "/". Without it the argument is recognized as an invalid expression and will not cause anything to happen.
You can get additional help by typing "reghack /?" without quotes.
In case you want to launch this using a shortcut the command /pause:e can be used