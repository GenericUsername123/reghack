﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;

namespace RegHack
{
  public class clsProgrammingTools
  {
    public static void RemoveEscape(ref string[] args)
    {
      var temp = new string[0];
      int index = 0;
      for (int i = 0; i < args.Length; i++)
      {
        string[] newArgs = args[i].Split(new string[] { " " }, StringSplitOptions.None);
        if (newArgs.Length > 1)
        {
          Array.Resize(ref temp, temp.Length + newArgs.Length);
          for (int j = 0; j < newArgs.Length; j++)
          {
            newArgs[j] = Regex.Replace(newArgs[j], "\"", "");
            temp[index] = newArgs[j];
            index++;
          }
        }
        else { Array.Resize(ref temp, index + 1); temp[index] = args[i]; index++; }
      }
      args = new string[temp.Length];
      for (int i = 0; i < temp.Length; i++) args[i] = temp[i];
    }

    public static string[] LowerCase(ref string[] original)
    {
      var clone = new string[original.Length];
      Array.Copy(original, clone, original.Length);
      for (int i = 0; i < original.Length; i++) original[i] = original[i].ToLower();
      return clone;
    }

    public static bool CompareByteArrays(byte[] a1, byte[] a2)
    {
      if (a1.Length == a2.Length)
      {
        for (int iArray = 0; iArray < a1.Length; iArray++)
          if (a1[iArray] != a2[iArray]) return false;
      }
      else return false;
      return true;
    }
  }
}
