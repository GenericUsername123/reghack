﻿using System.IO;

namespace RegHack
{
  internal class clsRegHack
  {
    internal enum Option { descriptionsS, descriptionsA, descriptions, regBackupText, logOff, tkown }

    internal static string[] GetText(Option option)
    {
      string[] txtToReturn = { "" };
      switch (option)
      {
        case Option.descriptionsS:
          txtToReturn = new string[6];
          txtToReturn[0] = "\tEnable or disable shortcut text from the \"New shortcut\" option\n\t\tin the contextmenu.";
          txtToReturn[1] = "Add an option to the contextmenu to take ownership.";
          txtToReturn[2] = "Disable or enable automatic backups.\n\t\tIMPORTANT: use this before any command on the same " +
            "line!\n\t\tBackups are made only of the registry.";
          txtToReturn[3] = "";
          txtToReturn[4] = "Cleans the backup folder.\n\t\tOnly the most recent files will be left.";
          txtToReturn[5] = "Force windows to use the casing you want on folders and files.";
          break;
        case Option.descriptionsA:
          txtToReturn = new string[6];
          txtToReturn[0] = "Open any file and/or folder with your own text editor.";
          txtToReturn[1] = "A ResourcesExtract hack: add an option to the contextmenu of\n\t\tall compatible files to " +
            "open with ResourcesExtract.";
          txtToReturn[2] = "\tChange Windows to open your own image editor from the edit\n\t\toption in your contexmenu.";
          txtToReturn[3] = "\tChange Windows to open your own text editor from the edit\n\t\toption in your contexmenu.";
          txtToReturn[4] = "Recover any registry backup specified.";
          txtToReturn[5] = "Export the specified registry hive key.";
          break;
        case Option.descriptions:
          txtToReturn = new string[8];
          txtToReturn[0] = "Source: Specify the source of your text editor.";
          txtToReturn[1] = "File: Specify if the contextmenuoption should be\n\t\t\tavailable when clicking a file.";
          txtToReturn[2] = "Directory: Specify if the contextmenuoption should be\n\t\t\tavailable when clicking a folder.";
          txtToReturn[3] = "Icon: Specify the iconpath of your text editor.";
          txtToReturn[4] = "Source: Specify the source of ResourcesExtract";
          txtToReturn[5] = "Icon: Specify the iconpath of ResourcesExtract.\n\t\t\t(An icon will be exported and used " + 
            "when this attribute\n\t\t\tis not present.)";
          txtToReturn[6] = "Source: Specify the source of your image editor.";
          break;
        case Option.regBackupText:
          txtToReturn[0] = "DO NOT edit any registry file, expecially not the name or extension!\nChanging any registry " +
            "file may cause loss of data during a maintenance or restoring the wrong files.\n" + typeof(Program).Namespace 
            + " uses timestamps of creation/last write to restore/delete files.";
          break;
        case Option.logOff:
          txtToReturn[0] = "\nIn order for the changes to take effect you will need to log off.";
          break;
        case Option.tkown:
          txtToReturn = new string[2];
          txtToReturn[0] = "cmd.exe /c takeown /f \"%1\" && icacls \"%1\" /grant administrators:F";
          txtToReturn[1] = "cmd.exe / c takeown / f \"%1\" /r /d y && icacls \"%1\" /grant administrators:F /t";
          break;
      }
      return txtToReturn;
    }

    internal static string CheckForFiles(string arg, string[] files, string[] types, int[] optional = null)
    {
      if (files.Length == types.Length)
      {
        int iOptional = 0;
        bool isOptional = false;
        for (int iFiles = 0; iFiles < files.Length; iFiles++)
        {
          if (optional != null)
            if (optional.Length > iOptional)
              if (optional[iOptional] == iFiles) { isOptional = true; iOptional++; }
          //Check if filetype is valid
          if (files[iFiles] != null)
            if (files[iFiles].Substring(files[iFiles].LastIndexOf(".") + 1) != types[iFiles])
            { Program.errCode = Program.errCodes.invalidFiletype; return arg; }
          //Check if file exists
          if (!isOptional && files[iFiles] == null) { Program.errCode = Program.errCodes.invalidUseAttr; return arg; }
          else if (!File.Exists(files[iFiles]) && files[iFiles] != null) { Program.errCode = Program.errCodes.invalidFilepath; return arg; }
          //Prepare for next
          isOptional = false;
        }
        return null;
      }
      else { Program.errCode = Program.errCodes.unknown; return null; }
    }
  }
}
